package com.oppo.examples;

import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;

public class DraftTest extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		// String str[] = { "1111", "22222", "333333" };
		// ListView lv=(ListView) super.findViewById(R.id.directory);
		// lv.setAdapter(new
		// ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,str));
		// super.setContentView(lv);
		DisplayMetrics dm = getResources().getDisplayMetrics();
		final int screenWidth = dm.widthPixels;
		final int screenHeight = dm.heightPixels - 50;

		final Button b = (Button) findViewById(R.id.btn);
		/*
		 * 点击事件
		 */
		b.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				b.getTop();
				showPopupWindow(0, 0);
			}
		});

		/*
		 * 拖动事件
		 */
		b.setOnTouchListener(new OnTouchListener() {
			int lastX, lastY;

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				int ea = event.getAction();
				Log.i("TAG", "Touch:" + ea);

				// Toast.makeText(DraftTest.this, "位置："+x+","+y,
				// Toast.LENGTH_SHORT).show();

				switch (ea) {
				case MotionEvent.ACTION_DOWN:
					lastX = (int) event.getRawX();
					lastY = (int) event.getRawY();
					break;
				/**
				 * layout(l,t,r,b) l Left position, relative to parent t Top
				 * position, relative to parent r Right position, relative to
				 * parent b Bottom position, relative to parent
				 * */
				case MotionEvent.ACTION_MOVE:
					int dx = (int) event.getRawX() - lastX;
					int dy = (int) event.getRawY() - lastY;
					int left = v.getLeft() + dx;
					int top = v.getTop() + dy;
					int right = v.getRight() + dx;
					int bottom = v.getBottom() + dy;
					if (left < 0) {
						left = 0;
						right = left + v.getWidth();
					}
					if (right > screenWidth) {
						right = screenWidth;
						left = right - v.getWidth();
					}
					if (top < 0) {
						top = 0;
						bottom = top + v.getHeight();
					}
					if (bottom > screenHeight) {
						bottom = screenHeight;
						top = bottom - v.getHeight();
					}
					v.layout(left, top, right, bottom);
					Log.i("", "position：" + left + ", " + top + ", " + right
							+ ", " + bottom);
					lastX = (int) event.getRawX();
					lastY = (int) event.getRawY();
					break;
				case MotionEvent.ACTION_UP:
					break;
				}
				return false;
			}
		});

	}

	private PopupWindow popupWindow;
	private LinearLayout layout;
	private ListView listView;
	
	private String title[] = { "全部", "我的微博", "周边", "智能排版", "同学","全部", "我的微博", "周边", "智能排版", "同学" };

//	@SuppressLint("NewApi")
	public void showPopupWindow(int x, int y) {
		layout = (LinearLayout) LayoutInflater.from(DraftTest.this).inflate(
				R.layout.dialog, null);
		listView = (ListView) layout.findViewById(R.id.lv_dialog);
		listView.setAdapter(new ArrayAdapter<String>(DraftTest.this,
				R.layout.text, R.id.tv_text, title));
		popupWindow = new PopupWindow(DraftTest.this);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		popupWindow
				.setWidth(getWindowManager().getDefaultDisplay().getWidth() / 2);
		popupWindow.setHeight(getWindowManager().getDefaultDisplay().getHeight() / 2);
		popupWindow.setAnimationStyle(R.style.AnimationPreview);
		popupWindow.setOutsideTouchable(true);
		popupWindow.setFocusable(true);
		popupWindow.setContentView(layout);
		// showAsDropDown会把里面的view作为参照物，所以要那满屏幕parent
		// popupWindow.showAsDropDown(findViewById(R.id.tv_title), x, 10);
		popupWindow.showAtLocation(findViewById(R.id.main),
				Gravity.CENTER_HORIZONTAL, x, y);// 需要指定Gravity，默认情况是center.
		/*
		 * list点击后事件处理
		 */
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				
				
				
				popupWindow.dismiss();
				popupWindow = null;
			}
		});
	}
}